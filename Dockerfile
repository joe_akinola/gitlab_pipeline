FROM docker:latest

####----Environment Variable---####
ENV NAMESPACE=toba-test
ENV TERRAFORM_VERSION=1.1.2
ENV AWS_ACCESS_KEY_ID=AKIA6AFPILQR4RLJD6OR
ENV AWS_SECRET_ACCESS_KEY=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
ENV AWS_DEFAULT_REGION=eu-west-2

#### Install Tools ####
RUN apk update && \
    apk add curl jq python3 bash ca-certificates git openssl unzip wget && \
    cd /tmp && \

#### Install Helm tool
    wget https://get.helm.sh/helm-v3.5.3-linux-amd64.tar.gz && \
    tar -zxvf helm-v3.5.3-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    rm -rf /tmp/* && \
    rm -rf /var/cache/apk/* && \
    rm -rf /var/tmp/*



#### Install Kubectl ####


RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin

#### Install AWS-CLI ####

RUN apk add --no-cache \
        py3-pip \
    && pip3 install --upgrade pip

RUN pip3 --no-cache-dir install --upgrade awscli
# Dependency module for kubernetes
    
RUN mkdir ~/.kube && mkdir ~/.aws 

###### Prepare SSH for AWSCLI ######
RUN mkdir ~/.ssh
WORKDIR /data

# COPY keys to /root/.ssh
RUN chown -R root /root/.ssh && \
    chmod -R 0400 /root/.ssh
RUN aws --version

#### update kubeconfig to cluster
RUN aws eks --region eu-west-2 update-kubeconfig --name sandbox-terraform

# ### Install chart museum---###
# RUN helm install --name my-chartmuseum chartmuseum/chartmuseum  -f /cert-manager/values.yaml -n ${NAMESPACE}

## Install Kustomize ###
RUN curl --silent --location --remote-name \
"https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v3.2.3/kustomize_kustomize.v3.2.3_linux_amd64" && \
chmod a+x kustomize_kustomize.v3.2.3_linux_amd64 && \
mv kustomize_kustomize.v3.2.3_linux_amd64 /usr/local/bin/kustomize
